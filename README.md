This repository contains the source code for modeling Covid-19 incidence rate evolution trend using Dual wave Gaussian-Lorentzian Composite (GLC) functions. 
The GLC function is a mathematical model derived by a combination of Gaussian and Lorentzian functions. The code estimates the trajectory of Covid19 incidences
across 20 different countries over a 180 day period which is customizable.

To run the code in command line:
python covid_glc.py -c "country_of_interest"

Eg. python covid_glc.py -c "US"

Note: Please refer to the JSON file to get the exact name of the countries.

Installation:
The source code is developed using Python 3.7.3. Following additional packages are required:

numpy==1.16.4
matplotlib==3.1.3
lmfit==1.0.0
tabulate==0.8.3
pandas==0.24.2
SALib==1.3.11

Data:
The Covid19 dataset is directly fetched by the main code from the COVID-19 Data Repository by the Center for Systems Science and 
Engineering (CSSE) at Johns Hopkins University at https://github.com/CSSEGISandData/COVID-19. The dataset specifically used for modeling is 
time_series_covid19_confirmed_global.csv. Full path to the file is in the JSON file. The data is updated on a daily frequency by CSSE.

Related Publications:
The publication is titled "Covid-19 Incidence Rate Evolution Modeling using Dual Wave Gaussian-Lorentzian Composite Functions".

License: MIT

Contact
Radhakrishnan Poomari: rk@xformics.com