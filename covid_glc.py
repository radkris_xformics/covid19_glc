#!~/anaconda3/envs/tensorflow/bin/python
# coding: utf-8
# Author: Radhakrishnan Poomari
# Alias: rk@xformics.com

"""
Dual Wave Gaussian-Lorentzian Composite (GLC) Model:

This module enables generation of GLC model based on a country's
Covid-19 incidence data.

Routines in this module:
_ts_index_generator(start, end=None, n=None, freq="D"): Generates time
series date index for incidence data

_reindex(df): Reindexes a given dataframe with forecast time horizon

_glc_model(pars, x, m=0.2): Generates the GLC model based on Gaussian
and Lorentzian parameters of first and second wave components

_residual(pars, x, data): Calculates the GLC model residual for best fit
estimation

_glc_fit(x_data, df_reindexed, h_1, mu_1, sigma_1, h_2, mu_2, sigma_2):
Fits the GLC model to incidence rate data

_plot_glc(country, output_dir, infections_total, df_reindexed,
              glc_fit_1, glc_fit_2, glc_composite_func, glc_model_fit):
Plots the GLC curves for first and second wave components, GLC best fit
curve and GLC model curve enveloping first and second wave components

_sobol_plot(si_df, output_dir, country):
Plots the Sobol sensitivity indices for parameters of first and second wave
component for the country of interest
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
import argparse
import warnings
from lmfit import Minimizer, Parameters
from SALib.sample import saltelli
from SALib.analyze import sobol
import matplotlib.colors
from tabulate import tabulate
warnings.filterwarnings("ignore")


# Generate time series index
def _ts_index_generator(start, end=None, n=None, freq="D"):
    """
    :param start: Start date of incidence data
    :param end: End date of incidence data. Defaults to None.
    :param n: Time period to generate the index
    :param freq: Forecast horizon in days
    :return: Time indexed data frame
    """
    if end is not None:
        index = pd.date_range(start=start, end=end, freq=freq)
    else:
        index = pd.date_range(start=start, periods=n, freq=freq)
    index = index[1:]
    print("--- Generating time series index --> start:", index[0],
          "| end:",
          index[-1], "| len:", len(index), "---")
    return index


def _reindex(df):
    """
    :param df: Input dataframe to be reindexed to include forecast horizon
    :return:  Reindexed dataframe
    """
    future_index = _ts_index_generator(start=df.index[-1], n=config[
        country_of_interest]['general_forecast_inputs']['forecast_steps_days'],
                                       freq='D')
    past_future_index = df.index.to_list() + future_index.to_list()
    index_new = pd.Series(past_future_index)
    df_reindexed = df.reindex(index_new)
    return future_index, df_reindexed


def _glc_model(pars, x, m=0.2):
    """
    :param pars: Parameters of Gaussian and Lorentzian functions as ordered
    dict
    :param x: Incidence rate data
    :param m: Gaussian-Lorentzian blending constant
    :return: GLC model
    """
    f_gaussian_1 = pars['amp_g_1'] * (1 - m) * np.exp(-((x - pars[
        'cen_g_1']) / pars['wid_g_1']) ** 2)
    f_lorentian_1 = pars['amp_l_1'] * pars['wid_l_1'] ** 2 * m / (pars[
                                                                      'wid_l_1'] ** 2 + (
                                                                              x -
                                                                              pars[
                                                                                  'cen_l_1']) ** 2)
    f_gaussian_2 = pars['amp_g_2'] * (1 - m) * np.exp(-((x - pars[
        'cen_g_2']) / pars['wid_g_2']) ** 2)
    f_lorentian_2 = pars['amp_l_2'] * pars['wid_l_2'] ** 2 * m / (pars[
                                                                      'wid_l_2'] ** 2 + (
                                                                              x -
                                                                              pars[
                                                                                  'cen_l_2']) ** 2)
    glc_1 = f_gaussian_1 + f_lorentian_1
    glc_2 = f_gaussian_2 + f_lorentian_2
    composite_model = glc_1 + glc_2
    return glc_1, glc_2, composite_model


def _residual(pars, x, data):
    """
    :param pars: Parameters of Gaussian and Lorentzian functions as ordered
    dict
    :param x:  Incidence rate data
    :param data: An array whose length is equal to the length of the
    incidence rate data
    :return: Residuals computed using Nelder-Mead and LM techniques
    """
    _, _, model = _glc_model(pars, x)
    return model - data


def _glc_fit(x_data, df_reindexed, h_1, mu_1, sigma_1, h_2, mu_2, sigma_2):
    """
    :param x_data: An array whose length is equal to the length of the
    incidence rate data
    :param df_reindexed: Reindexed dataframe of incidence data
    :param h_1: Amplitude of first wave component
    :param mu_1: Center of first wave component
    :param sigma_1:  Standard deviation of first wave component
    :param h_2: Amplitude of second wave component
    :param mu_2: Center of second wave component
    :param sigma_2: Standard deviation of second wave component
    :return: Returns the residuals of GLC model, the best fit values of GLC
    model, output of first and second wave components and overall model
    output of GLC enveloping first and second wave components
    """
    pfit = Parameters()
    pfit.add(name='amp_g_1', value=h_1)
    pfit.add(name='amp_l_1', value=h_1)
    pfit.add(name='cen_g_1', value=mu_1)
    pfit.add(name='cen_l_1', value=mu_1)
    pfit.add(name='wid_g_1', value=sigma_1)
    pfit.add(name='wid_l_1', value=sigma_1)
    pfit.add(name='amp_g_2', value=h_2)
    pfit.add(name='amp_l_2', value=h_2)
    pfit.add(name='cen_g_2', value=mu_2)
    pfit.add(name='cen_l_2', value=mu_2)
    pfit.add(name='wid_g_2', value=sigma_2)
    pfit.add(name='wid_l_2', value=sigma_2)
    mini = Minimizer(_residual, pfit, fcn_args=(x_data, df_reindexed))
    result_1 = mini.minimize(method='Nelder')
    result_2 = mini.minimize(method='leastsq', params=result_1.params)
    # Non-linear curve fitting with step ahead date index
    df_residual = pd.Series(result_2.residual)
    df_residual.index = dtf['new'].index
    glc_residual_fit = dtf_reindexed + df_residual
    # Fit GLS model on original data with step ahead date index
    y_glc_1_forecast, y_glc_2_forecast, y_glc_fit = _glc_model(
        pfit, X_new)
    return pfit, glc_residual_fit, y_glc_1_forecast, y_glc_2_forecast, y_glc_fit


def _plot_glc(country, output_dir, infections_total, df_reindexed,
              glc_fit_1, glc_fit_2, glc_composite_func, glc_model_fit):
    """
    :param country: Country of interest
    :param output_dir: Output directory to save plots
    :param infections_total: Display total number of incidences till date
    from public data source
    :param df_reindexed: Reindexed dataframe
    :param glc_fit_1: Model out of first wave component
    :param glc_fit_2: Model out of second wave component
    :param glc_composite_func: Dual wave GLC model output of first and second
    components
    :param glc_model_fit: Best fit model based on both first and second wave components
    :return: Plot of curves and their evolutionary trajectories
    """
    plt.style.use('seaborn')
    chart_label = country + ' Covid-19 Incidence Rate Evolution Trend'
    fig, ax = plt.subplots(dpi=150)
    p1 = plt.plot(df_reindexed.index, df_reindexed, color='red', marker="o",
                  markersize=6, linestyle=None,
                  label='Total no. of Incidences/Week')
    chart_box_0 = ax.get_position()
    ax.set_position([chart_box_0.x0,
                     chart_box_0.y0,
                     chart_box_0.width,
                     chart_box_0.height])
    ax.legend(p1,
              ('Incidence Rate',
               'No. of Deaths',
               'No. of Recoveries'),
              fontsize=14,
              ncol=1,
              framealpha=0,
              fancybox=True,
              loc='upper right',
              shadow=True)
    ax.plot(df_reindexed.index, glc_composite_func, color='blue', label='GLC Model best fit')
    ax.plot(df_reindexed.index, glc_fit_1, color='green', label='GLC Model '
                                                                'for wave component 1')
    ax.plot(df_reindexed.index, glc_fit_2, color='darkorange', label='GLC '
                                                                     'Model for wave component 2')
    ax.plot(df_reindexed.index, glc_model_fit, color='black',
            label='GLC Model')
    ax.fill_between(df_reindexed.index, glc_model_fit,
                    color='lightsteelblue', alpha=0.7)
    chart_box_1 = ax.get_position()
    ax.set_position([chart_box_1.x0,
                     chart_box_1.y0,
                     chart_box_1.width,
                     chart_box_1.height])
    ax.legend(loc='upper right', shadow=True, ncol=1, prop={'size': 10})
    ax.set_title('Total no. of Incidences from Jan 22, 2020 till date: %d'
                 % infections_total, fontsize=16)
    ax.set_xlabel('Covid-19 Incidence Rate timeline', fontsize=16)
    ax.tick_params(axis='x', labelsize=12)
    ax.set_ylabel('New Covid-19 incidences per week', fontsize=16)
    ax.tick_params(axis='y', labelsize=12)
    fig.suptitle(chart_label, fontsize=20)
    plt.gcf().autofmt_xdate()
    fig.savefig(output_dir + country + '.png', format="png")
    plt.show()


def _sobol_plot(si_df, output_dir, country):
    """
    :param si_df: Estimated Sobol Sensitivity Indices (SSI)
    :param output_dir: Directory to save plot
    :param country: Country of interest
    :return: Grouped bar plot of SSI for each of the GLC model parameters
    """
    levels = [0, 1, 2, 3, 4]
    colors = ['goldenrod', 'tomato', 'cornflowerblue', 'green']
    cmap, norm = matplotlib.colors.from_levels_and_colors(levels, colors)
    ax = si_df.plot.bar(colormap=cmap, alpha=0.8, figsize=(6, 8))
    fig = ax.get_figure()
    chart_box_1 = ax.get_position()
    ax.set_position([chart_box_1.x0,
                     chart_box_1.y0,
                     chart_box_1.width,
                     chart_box_1.height])
    ax.set_xlabel('Gaussian-Lorentzian Composite Function Parameters',
                  fontsize=16)
    ax.set_ylabel('Sobol Indices', fontsize=16)
    ax.set_xticklabels(
        ('amp_g_1', 'amp_l_1', 'cen_g_1', 'cen_l_1', 'wid_g_1',
         'wid_l_1', 'amp_g_2', 'amp_l_2', 'cen_g_2', 'cen_l_2', 'wid_g_2',
         'wid_l_2'), rotation=90, ha='right')
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    ax.legend(loc='upper left', shadow=True, ncol=1, prop={'size': 12})
    fig.suptitle('Sobol Sensitivity Indices for ' + country, fontsize=20)
    fig.set_size_inches(20, 15)
    plt.gcf().subplots_adjust(bottom=0.20)
    fig.savefig(output_dir + country + '_si_plot' + '.png', format="png")
    plt.show()


if __name__ == "__main__":
    jsonfile = r'covid.json'
    json_file = open(jsonfile)
    config = json.load(json_file)

    # Command line Argument Parse to get Country of Interest
    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--country", required=True,
                    help="Enter country of interest")
    args = vars(ap.parse_args())

    country_of_interest = args["country"]
    dtf = pd.read_csv(config['global_infections'], sep=",")
    chart_dir = config['chart_dir']

    # Standardize Country/Province renaming and classification
    dtf['Country/Region'].loc[dtf['Province/State'] == 'Hong Kong'] = 'Hong Kong'
    dtf['Country/Region'].loc[dtf['Country/Region'] == 'Taiwan*'] = 'Taiwan'
    dtf['Country/Region'].loc[dtf['Country/Region'] == 'Korea, South'] = 'South ' \
                                                                         'Korea'

    # Group by country
    dtf = dtf.drop(['Province/State', 'Lat', 'Long'], axis=1).groupby(
        "Country/Region").sum().T

    # Convert index to datetime
    dtf.index = pd.to_datetime(dtf.index, infer_datetime_format=True)

    # Create total incidences column
    dtf = pd.DataFrame(index=dtf.index, data=dtf[country_of_interest].values,
                       columns=["total"])

    # Converting daily incidences to weekly incidence rate
    dtf["new"] = dtf["total"] - dtf["total"].shift(7)
    dtf["new"] = dtf["new"].fillna(method='bfill')

    total_infections = dtf['total'].iloc[-1]
    curve_peak_date = dtf[['new']].idxmax()
    infection_origin_date = dtf.index[0]
    time_to_peak = curve_peak_date - infection_origin_date
    time_to_peak_days = time_to_peak.dt.days

    # Reindex timestamps
    future_dateindex, dtf_reindexed = _reindex(dtf['new'])
    dtf_reindexed_dropna = dtf_reindexed.dropna()
    X_dropna = np.arange(len(dtf_reindexed.dropna()))
    X_new = np.arange(len(dtf_reindexed))
    future_days = np.setdiff1d(X_new, X_dropna)

    #########
    # Wave 1 #
    #########
    # Constant coefficients for Guassian and Lorentian Functions
    nmi_delay_factor = config[country_of_interest][
        'general_forecast_inputs']['nmi_delay_factor']
    h1_scaling_factor = config[country_of_interest]['wave_1_parameters'][
        'h1_scaling_factor']
    sigma1_multiplier = config[country_of_interest]['wave_1_parameters'][
        'sigma1_multiplier']

    # Parameters of first wave component
    h1 = max(dtf['new']) * h1_scaling_factor
    mu1 = time_to_peak_days[0] + nmi_delay_factor
    sigma1 = (time_to_peak_days[0] / 7) * sigma1_multiplier

    #########
    # Wave 2 #
    #########
    # Scenario where peak of Wave 2 is higher than Wave 1
    mu2_scaling_factor = config[country_of_interest]['wave_2_parameters'][
        'mu2_scaling_factor']
    if config[country_of_interest]['wave_2_parameters']['wave_type'] == 'primary':
        mu2 = (time_to_peak_days[0] - time_to_peak_days[
            0] / mu2_scaling_factor) + nmi_delay_factor
        h2_scaling_factor = config[country_of_interest]['wave_2_parameters'][
            'h2_scaling_factor']
        sigma2_multiplier = config[country_of_interest]['wave_2_parameters'][
            'sigma2_multiplier']
        h2 = h1 / h2_scaling_factor
        sigma2 = sigma2_multiplier * sigma1
    # Scenario where peak of Wave 1 is higher than wave 2
    else:
        mu2 = time_to_peak_days[0] + ((time_to_peak_days[
            0]) / mu2_scaling_factor) + nmi_delay_factor
        h2_scaling_factor = config[country_of_interest]['wave_2_parameters'][
            'h2_scaling_factor']
        h2 = h1 / h2_scaling_factor
        sigma2_multiplier = config[country_of_interest]['wave_2_parameters'][
            'sigma2_multiplier']
        sigma2 = sigma2_multiplier * sigma1

    params, gls_residual, y_gls_1, y_gls_2, y_gls_model_fit = _glc_fit(X_dropna,
                                                                       dtf_reindexed_dropna,
                                                                       h1, mu1, sigma1, h2, mu2, sigma2)

    # Plot of GLC, GLC best fit optimized model and individual wave
    # components with step ahead date index

    _plot_glc(country_of_interest, chart_dir, total_infections,
              dtf_reindexed, y_gls_1, y_gls_2,
              gls_residual, y_gls_model_fit)

    # Sobol Sensitivity Analysis (SSA)
    problem = {'num_vars': 6, 'names': ['amp_g_1', 'cen_g_1', 'wid_g_1',
                                        'amp_g_2', 'cen_g_2', 'wid_g_2'],
               'bounds': [[h1, h1 * 5], [mu1, mu1+5], [sigma1, sigma1+5],
                          [h2, h2 * 5], [mu2, mu2+5], [sigma2, sigma2+5]]}

    # Generate samples
    param_values = saltelli.sample(problem, 5)
    w, h = X_dropna.shape[0], param_values.shape[0]
    y_glc = [[0 for x in range(w)] for y in range(h)]

    # Parameter sample generator using Saltelli sampler
    params_table = [["Amplitude of Wave 1", h1],
                    ["Center of Wave 1", mu1],
                    ["Standard Deviation of Wave 1", sigma1],
                    ["Amplitude of Wave 2", h2],
                    ["Center of Wave 2", mu2],
                    ["Standard Deviation of Wave 1", sigma2],
                    ["Total no. of models generated for Sensitivity "
                     "Analysis", len(param_values)],
                    ["Time to compute SA for models",
                     (len(param_values)*4)/60]]
    print(tabulate(params_table, tablefmt='grid'))

    # Generate models using parameter ranges for SSA
    for i, X in enumerate(param_values):
        print("Generating model no:...........", i)
        _, _, _, _, y_glc[i] = _glc_fit(X_dropna, dtf_reindexed_dropna,
                                        X[0], X[1], X[2], X[3], X[4], X[5])
        print('Completed fitting glc for model....', i)
    y_glc_array = np.asarray(y_glc)
    y_pred = np.asarray([item[-1] for item in y_glc_array])

    # Compute Sobol Sensitivity Indices (SSI)
    Si = sobol.analyze(problem, y_pred, print_to_console=True, parallel=True)
    df_Si = pd.DataFrame.from_dict(Si, orient='index').transpose()
    _sobol_plot(df_Si, chart_dir, country_of_interest)
